#!/usr/bin/python

import os, sys

"""
A simple Python Project Directory Cleaner to remove "pyc" files.
"""

__author__ = 'Amyth Arora (amyth@techstricks.com)'

plst = []

def usage():
    print "Usage: %s <dirname>" % sys.argv[0]

def get_pycs(directory):
    rdirs = []
    dirname = os.path.realpath(directory)
    for e in os.listdir(directory):
        if os.path.isdir(os.path.join(directory, e)):
            rdirs.append(os.path.join(directory, e))
        elif os.path.isfile(os.path.join(directory, e)):
            if e[-3:] == 'pyc':
                os.remove(os.path.join(directory, e))
                plst.append('1')
            else:
                pass
        else:
            pass
    #Remove Pycs from Subdirs
    for d in rdirs:
        get_pycs(d)

def main(argv):
    if len(argv) != 1:
        usage()
        sys.exit(1)
    get_pycs(sys.argv[1])
    print 'Finished Processing. Deleted %d files' % len(plst)

if __name__ == '__main__':
    main(sys.argv[1:])
